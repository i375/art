# Art & CG Experiments #

## Still ##

[![art_sprint_2](arts_sprint_2/icon.png)](https://bitbucket.org/i375/arts_sprint_2/src/master/render/render1.png)
[![LoudSpeaker1](LoudSpeaker1/icon.png)](LoudSpeaker1/render.png)
[![ThreeAxisFlatShading](ThreeAxisFlatShading/icon.png)](ThreeAxisFlatShading/render.png)
[![Bastilia](Bastilia/icon.png)](Bastilia/render.png) 
[![CompanionCube19](CompanionCube19/icon.png)](CompanionCube19/render.png)
[![Redsun11](Redsun11/icon.png)](Redsun11/render.png)
[![Castle33](Castle33/icon.png)](Castle33/render.png)
[![Gate12](Gate12/icon.png)](Gate12/render.png)
[![Cubes](Cubes/icon.png)](Cubes/render.png)
[![Pallete13062015](Pallete13062015/icon.png)](Pallete13062015/render.png)
[![dirigible](dirigible/icon.png)](dirigible/render.png)
[![Cube8](Cube8/icon.png)](Cube8/render.png)
[![BorjomiRenders](BorjomiRenders/icon.png)](BorjomiRenders/render.png)
[![Cafe7](Cafe7/icon.png)](Cafe7/render.png)
[![NuclearTrainInMiddleofnowhere](NuclearTrainInMiddleofnowhere/icon.png)](NuclearTrainInMiddleofnowhere/render.png)
[![Fireplace79](Fireplace79/icon.png)](Fireplace79/render.png)
[![CubeGrid1](CubeGrid1/icon.png)](CubeGrid1/render.png)

## Animated ##

[![TrackRacer19](TrackRacer19/icon.png)](TrackRacer19/render.gif)
[![TheMill](TheMill/icon.png)](TheMill/render.gif)
[![StraightRoad](StraightRoad/icon.png)](StraightRoad/render.gif)